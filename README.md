# tarteaucitron
Portage pour SPIP de la solution de gestion de consentement aux cookies Tarteaucitron.js

Voir https://www.cnil.fr/fr/solutions-centralisees-de-recueil-de-consentement-aux-cookies-les-gestionnaires-de-tag

La documentation est ici : https://contrib.spip.net/Tarteaucitron-5292

## Ajouter un service
1. Rendez-vous sur ***ecrire/?exec=configurer_services*** pour rechercher votre service et l'activer. Au besoin, renseignez les paramètres nécessaires dans la liste des services activés

2. Recherchez votre service sur https://tarteaucitron.io/fr/install/ (Etape 3 : ajouter les services) et pensez à supprimer le marqueur du service dans vos squelettes s'il est présent. Si le marqueur est situé dans du contenu éditorial (le texte d'un article par exemple), passez à la suite.

### Marqueurs situés dans du contenu éditorial
Vous pouvez ajouter le marqueur de certains services dans du contenu éditorial grâce aux modèles fournis par le plugin (voir le dossier ***/modeles*** du plugin). Si l'entrée n'existe pas dans le porte-plume, il suffit de créer une icône carrée de 17px de côté dans ***/squelettes/icones_barre/tac_mon_modele.png***. Si des articles antérieurs contiennent des iframes pour ces services, il faut traiter le contenu éditorial ancien.

#### Traiter du contenu édiorial ancien
Pour traiter du contenu ancien, adaptez le fichier ***action/tarteaucitron_nettoyer_iframes.php*** en fonction de vos besoins et appelez-le via le bouton de l'onglet technique (***ecrire/?exec=configurer_technique***) ou directement par l'url ***spip.php?action=tarteaucitron_nettoyer_iframes***. Il s'agit d'une adaptation du script présent dans le plugin **oembed**.