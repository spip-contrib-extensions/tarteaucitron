<?php
if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Un simple formulaire de config,
 * on a juste à déclarer les saisies
 **/
function formulaires_configurer_tarteaucitron_panneau_saisies_dist() {
	// $saisies est un tableau décrivant les saisies à afficher dans le formulaire de configuration
	$saisies = array(
		array(
			'saisie' => 'textarea',
			'options' => array(
				'nom' => 'lang_disclaimer',
				'label' => '<:tarteaucitron:cfg_text_disclaimer:>',
				'explication' => '<:tarteaucitron:cfg_text_disclaimer_explication:>',
				'rows' => 3
			)
		),
		array(
			'saisie' => 'checkbox',
			'options' => array(
				'nom' => 'mandatory',
				'label' => '<:tarteaucitron:cfg_mandatory:>',
				'explication' => '<:tarteaucitron:cfg_mandatory_explication:>',
				'datas' => array(
					'true' => '<:item_oui:>'
				)
			)
		),
		array(
			'saisie' => 'checkbox',
			'options' => array(
				'nom' => 'moreInfoLink',
				'label' => '<:tarteaucitron:cfg_moreinfolink:>',
				'datas' => array(
					'true' => '<:item_oui:>'
				)
			)
		),
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'readmoreLink',
				'label' => '<:tarteaucitron:cfg_readmorelink:>',
				'explication' => '<:tarteaucitron:cfg_readmorelink_explication:>',
				'afficher_si' => '@moreInfoLink@ == "true"'
			)
		),
		array(
			'saisie' => 'checkbox',
			'options' => array(
				'nom' => 'groupServices',
				'label' => '<:tarteaucitron:cfg_group_services:>',
				'explication' => '<:tarteaucitron:cfg_group_services_explication:>',
				'datas' => array(
					'true' => '<:item_oui:>'
				)
			)
		),
		array(
			'saisie' => 'checkbox',
			'options' => array(
				'nom' => 'removeCredit',
				'label' => '<:tarteaucitron:cfg_remove_credit:>',
				'explication' => '<:tarteaucitron:cfg_remove_credit_explication:>',
				'attention' => '<:tarteaucitron:cfg_remove_credit_attention:>',
				'datas' => array(
					'true' => '<:item_oui:>'
				)
			)
		),
		array(
			'saisie' => 'hidden',
			'options' => array(
				'nom' => '_meta_casier',
				'defaut' => 'tarteaucitron'
			)
		)
	);
	return $saisies;
}